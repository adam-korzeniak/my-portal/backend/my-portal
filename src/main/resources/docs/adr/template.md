
# Need (context)
Why I need to choose something. What I need to achieve

# Possibilities
Possible solutions

# Analysis
Analysis of solutions, their capabilities, drawbacks

# Decision
Decision that was made with explanation why that decision was chosen
