Zapisanie decyzji, którą podjąłem, wraz z informacją dlaczego ją podjąłem
(np. kontekst, możliwe rozwiązania, wybrane rozwiązanie, wyjaśnienie)

# Need (context)

Need to persist data

# Possibilities

I considered following choices. In memory database were rejected, because they do not persist data.

- SQL
    - Postgres
    - Mysql
    - Microsoft SQL
    - Oracle Server
    - SqLite
    - MariaDb
    - H2
- NoSQL
    - Document DB
        - MongoDB
        - CouchDB
    - Key-Store
        - Redis
        - DynamoDB
        - S3
    - Wide-Column DBs
        - DynamoDB
        - Cassandra
    - Search Engines
        - ElasticSearch
        -
        - Firebase
        - Neo4j
        - OrientDB
        - IBM

# Analysis

## SQL vs NoSQL Database

SQL databases are easier to manage relations between objects.
SQL databases tend to have more tables than NoSQL collections which makes it harder to recognize data model.
SQL databases require schema, while NoSQL may have schema
SQL databases have better default support for transactions, only some NoSQL supports multi-document transactions (e.g.
MongoDB)
SQL databases are scaled vertically (larger server), NoSQL horizontally (multiple servers).
SQL databases require ORM, NoSQL not.

Should go for SQL

- Have many relations, especially using shared objects
- Have strict schema

Should go for NoSQL

- Relations can be contained within one collection (not many lookups and shared objects)
- Many relations that can be contained within one collection

## Choosing database based on requirements

### Entries and relations design

### Querying

_How will you search for data?_

#### Fetching data by key

Choose key-value store: DynamoDB, Redis, S3

#### Fetching mostly by key, but additional one or two fields

Wide-Column DBs (e.g.DynamoDB, Cassandra) may be right for you.

#### Query by many different fields

Relational DB: MySQL, PostgreSQL
Document DB: MongoDB, CouchDB, MySQL, PostgreSQL

#### Free text search

Search Engines: ElasticSearch

### Consistency

_Is strong consistency required or eventual consistency is OK?_

In case you need to read your data right after your write it (i.e. strong consistency) than a Relational database is
usually more suited than a Document Database, especially in case of multi-data-center scenario.

### Storage Capacity

_How much storage capacity is needed?_

Most database systems are limited by the amount of space on disk (e.g. MySQL) or struggle with performance as amount of
Nodes and Shards grows into the hundreds (e.g. Elasticsearch).

When infinite storage is needed this is where cloud solutions shine. Object Storage Services like S3 and GCS will allow
you to store as much data as you like with the handy option is multiple tiers, so you pay less for data that is rarely
retrieved.

### Performance

_What is the needed throughput and latency?_

All databases performance degrades as the amount of read/write throughput traffic increases.
This is the time when optimizations such as re-indexing and re-sharding of your data come in handy.

## Databases description

### SQL

#### Postgres

ACID: Fully
Concurrency control (MVCC): Yes
Indexes: Yes - trees, expression indexes, partial indexes, and hash indexes
Data types: Object-relational, can store objects with properties
Views: Supports advanced - Materialized views
Stored procedures: Supports, also written in other than SQL language
Triggers: INSTEAD OF - can run complex SQL statements using functions

#### MySQL

ACID: Only with InnoDB and NDB Cluster engines
Concurrency control (MVCC): No
Indexes: Yes - B-tree and R-tree
Data types: Relational
Views: Supported
Stored procedures: Supports
Triggers: BEFORE + AFTER for INSERT/UPDATE/DELETE

#### Microsoft SQL

#### Oracle

#### SqLite

#### MariaDb

### NoSQL

# Decision

I choose PostgresSQL.
Relational database is needed to manage relations between objects.

PostgresSQL is chosen to improve my knowledge about it, because I am using it in commercial project.

Otherwise I would choose

## Entries and relations design

We have some shared relations:

- Account Group - Account - OneToMany
- Transaction - Account - ManyToMany

If accounts groups could contain account I might consider NoSQL databases. But it is not great, because transactions are
related to accounts. And many more things in future will be related to accounts. So accounts and groups need to stay
disconnected.

## Querying

I need to query transactions by multiple dynamically selected fields. Therefore, Key-Value and Wide-Column databases are
rejected.

## Consistency

Not required, but useful

## Storage Capacity

Not important, volume will be very low

## Storage Capacity

Not important, only I will be using it