package me.korzeniak.myportal.shopping.domain.list.dto;

import lombok.Data;

@Data
public class CategoryReorderCommand {
    private long id;
    private int order;
}
