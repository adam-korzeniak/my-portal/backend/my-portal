package me.korzeniak.myportal.shopping.domain.list.outbound.db;

import me.korzeniak.myportal.shopping.domain.list.dto.CategoryQuery;
import me.korzeniak.myportal.shopping.domain.list.model.Category;
import me.korzeniak.myportal.shopping.domain.list.service.CategoryRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CategoryRepositoryImpl implements CategoryRepository {
    @Override
    public Optional<Category> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public List<Category> findAll() {
        return List.of();
    }

    @Override
    public List<Category> findByQuery(CategoryQuery query) {
        return List.of();
    }

    @Override
    public Optional<Category> findLastCategory() {
        return Optional.empty();
    }

    @Override
    public Category save(Category category) {
        return null;
    }

    @Override
    public Category moveToOrder(Category category, int order) {
        return null;
    }
}
