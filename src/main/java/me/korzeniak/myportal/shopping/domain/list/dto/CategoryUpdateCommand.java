package me.korzeniak.myportal.shopping.domain.list.dto;

import lombok.Data;

@Data
public class CategoryUpdateCommand {
    private long internalId;
    private String name;
}
