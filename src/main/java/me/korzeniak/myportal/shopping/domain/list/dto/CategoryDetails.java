package me.korzeniak.myportal.shopping.domain.list.dto;

import lombok.Data;

@Data
public class CategoryDetails {
    private String id;
    private String name;
    private int order;
}
