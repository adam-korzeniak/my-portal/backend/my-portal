package me.korzeniak.myportal.shopping.domain.list.dto;

import lombok.Data;

@Data
public class CategoryCreateCommand {
    private String name;
}
