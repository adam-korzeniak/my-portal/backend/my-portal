package me.korzeniak.myportal.shopping.domain.list.service;

import lombok.RequiredArgsConstructor;
import me.korzeniak.myportal.shopping.domain.list.dto.*;
import me.korzeniak.myportal.shopping.domain.list.model.Category;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public List<Category> getCategories(CategoryQuery query) {
        return categoryRepository.findByQuery(query);
    }

    public void createCategory(CategoryCreateCommand command) {
        int currentLastOrder = categoryRepository.findLastCategory()
                .map(Category::getOrder)
                .orElse(0);
        Category category = CategoryMapper.INSTANCE.mapFromCommand(command, currentLastOrder + 1);
        categoryRepository.save(category);
    }

    public void updateCategory(CategoryUpdateCommand command) {
        categoryRepository.findById(command.getInternalId())
                .map(category -> CategoryMapper.INSTANCE.update(category, command))
                .ifPresent(categoryRepository::save);
    }

    public void reorderCategory(CategoryReorderCommand command) {
        categoryRepository.findById(command.getId())
                .ifPresent(category -> categoryRepository.moveToOrder(category, command.getOrder()));
    }

}
