package me.korzeniak.myportal.shopping.domain.list.service;

import me.korzeniak.myportal.shopping.domain.list.dto.CategoryCreateCommand;
import me.korzeniak.myportal.shopping.domain.list.dto.CategoryUpdateCommand;
import me.korzeniak.myportal.shopping.domain.list.model.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface CategoryMapper {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    @Mapping(target = "internalId", ignore = true)
    Category mapFromCommand(CategoryCreateCommand categoryCreateCommand, int order);

    @Mapping(target = "internalId", ignore = true)
    @Mapping(target = "order", ignore = true)
    Category update(@MappingTarget Category category, CategoryUpdateCommand command);
}
