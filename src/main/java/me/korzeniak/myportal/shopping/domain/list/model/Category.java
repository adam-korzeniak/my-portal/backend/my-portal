package me.korzeniak.myportal.shopping.domain.list.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "category")
public class Category {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_id_sequence")
    @SequenceGenerator(name = "category_id_sequence", sequenceName = "category_id_sequence")
//    @Setter(AccessLevel.NONE)
    //TODO: Change to sequence and modify identifier as Value Object
    private Long internalId;

    private String name;

    private int order;
}
