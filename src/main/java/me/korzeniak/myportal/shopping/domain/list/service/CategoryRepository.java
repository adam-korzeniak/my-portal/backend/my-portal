package me.korzeniak.myportal.shopping.domain.list.service;

import me.korzeniak.myportal.shopping.domain.list.dto.CategoryQuery;
import me.korzeniak.myportal.shopping.domain.list.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {

    Optional<Category> findById(Long id);

    List<Category> findAll();

    List<Category> findByQuery(CategoryQuery query);

    Optional<Category> findLastCategory();

    Category save(Category category);

    Category moveToOrder(Category category, int order);
}
