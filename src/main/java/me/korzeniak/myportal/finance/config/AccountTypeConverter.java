package me.korzeniak.myportal.finance.config;

import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import me.korzeniak.myportal.shared.config.web.conversion.EnumConverter;
import org.springframework.core.convert.converter.Converter;

class AccountTypeConverter extends EnumConverter<AccountType> implements Converter<String, AccountType> {

    public AccountTypeConverter() {
        super(AccountType.class);
    }
}