package me.korzeniak.myportal.finance.shared;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

@NoArgsConstructor(access = AccessLevel.NONE)
public class PriceUtils {

    public static BigDecimal round(BigDecimal price) {
        return price.setScale(2, RoundingMode.HALF_UP);
    }
}
