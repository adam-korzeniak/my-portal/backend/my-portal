package me.korzeniak.myportal.finance.domain.transaction.dto;

import me.korzeniak.myportal.finance.domain.account.dto.AccountSimpleDto;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class TransactionDto {
    private UUID id;
    private TransactionType type;
    private AccountSimpleDto source;
    private AccountSimpleDto target;
    private BigDecimal amount;
    private LocalDateTime date;
    private String comment;
    private String shop;
}
