package me.korzeniak.myportal.finance.domain.transaction;

import me.korzeniak.myportal.finance.domain.account.AccountFacade;
import me.korzeniak.myportal.finance.domain.account.dto.AccountDto;
import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionSimpleDto;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionType;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class TransactionFacade {

    private final AccountFacade accountFacade;
    private final TransactionProvider transactionProvider;
    private final TransactionMapper transactionMapper;

    @Transactional
    public TransactionDto createTransaction(TransactionSimpleDto transactionDto) {
        TransactionDto dto = buildTransaction(transactionDto);
        return transactionProvider.createTransaction(dto);

    }

    @Transactional
    public Optional<TransactionDto> editTransaction(TransactionSimpleDto transactionDto) {
        TransactionDto dto = buildTransaction(transactionDto);
        return transactionProvider.editTransaction(dto);

    }

    private TransactionDto buildTransaction(TransactionSimpleDto transactionDto) {
        AccountDto source = getSourceAccount(transactionDto);
        AccountDto target = getTargetAccount(transactionDto);
        return transactionMapper.mapToExpandedDto(transactionDto, source, target);
    }

    private AccountDto getSourceAccount(TransactionSimpleDto transactionDto) {
        UUID sourceAccountId = transactionDto.getSourceAccountId();
        AccountType sourceAccountType = getSourceAccountType(transactionDto.getType());
        return accountFacade.getAccount(sourceAccountId, sourceAccountType)
                .orElseThrow();
    }

    private AccountDto getTargetAccount(TransactionSimpleDto transactionDto) {
        UUID targetAccountId = transactionDto.getTargetAccountId();
        AccountType targetAccountType = getTargetAccountType(transactionDto.getType());
        return accountFacade.getAccount(targetAccountId, targetAccountType)
                .orElseThrow();
    }

    private AccountType getSourceAccountType(TransactionType transactionType) {
        return switch (transactionType) {
            case INCOME -> AccountType.INCOME;
            case EXPENSE, TRANSFER -> AccountType.ASSET;
            case INITIAL -> AccountType.ASSET;
//            case INITIAL -> throw new RuntimeException();
        };

    }

    private AccountType getTargetAccountType(TransactionType transactionType) {
        return switch (transactionType) {
            case EXPENSE -> AccountType.EXPENSE;
            case INCOME, TRANSFER -> AccountType.ASSET;
            case INITIAL -> AccountType.ASSET;
//            case INITIAL -> throw new RuntimeException();
        };
    }

    public List<TransactionDto> getAllTransactions() {
        return transactionProvider.getAllTransactions();
    }

    public List<TransactionDto> getTransactions(int limit) {
        return transactionProvider.getTransactions(limit);
    }

    public void deleteTransaction(UUID id) {
        transactionProvider.deleteTransaction(id);
    }
}
