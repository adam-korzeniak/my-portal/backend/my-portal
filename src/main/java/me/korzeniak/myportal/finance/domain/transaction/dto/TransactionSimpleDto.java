package me.korzeniak.myportal.finance.domain.transaction.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class TransactionSimpleDto {
    private UUID id;
    private TransactionType type;
    private UUID sourceAccountId;
    private UUID targetAccountId;
    private BigDecimal amount;
    private LocalDateTime date;
    private String comment;
    private String shop;
}
