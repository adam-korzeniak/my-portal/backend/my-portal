package me.korzeniak.myportal.finance.domain.asset.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Data
public class AssetGroupDto {
    private UUID id;
    private String name;
    private BigDecimal amount = BigDecimal.ZERO;
    private List<AssetDto> assets = new ArrayList<>();

    public AssetGroupDto(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addAsset(AssetDto asset) {
        assets.add(asset);
    }

    public void addAmount(BigDecimal amount) {
        this.amount = this.amount.add(amount);
    }
}
