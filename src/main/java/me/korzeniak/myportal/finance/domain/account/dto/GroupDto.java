package me.korzeniak.myportal.finance.domain.account.dto;

import me.korzeniak.myportal.finance.shared.Default;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
public class GroupDto {
    private final UUID id;
    private final String name;
    private final AccountType type;
    private final Integer order;
    private final List<AccountSimpleDto> accounts;

    public GroupDto(String name, AccountType type) {
        this(UUID.randomUUID(), name, type, null, new ArrayList<>());
    }

    public GroupDto(UUID id, String name, AccountType type) {
        this(id, name, type, null, new ArrayList<>());
    }

    @Default
    public GroupDto(UUID id, String name, AccountType type, Integer order, List<AccountSimpleDto> accounts) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.order = order;
        this.accounts = accounts;
    }

    public void addAccount(AccountSimpleDto account) {
        this.accounts.add(account);
    }

}
