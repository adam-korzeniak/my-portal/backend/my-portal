package me.korzeniak.myportal.finance.domain.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@AllArgsConstructor
public class HierarchyDto {
    private final List<GroupDto> groups;

    public HierarchyDto() {
        this.groups = new ArrayList<>();
    }

    public void addGroup(GroupDto group) {
        this.groups.add(group);
    }

    public List<AccountSimpleDto> getAllAccounts() {
        return groups.stream()
                .map(GroupDto::getAccounts)
                .flatMap(Collection::stream)
                .toList();
    }

}
