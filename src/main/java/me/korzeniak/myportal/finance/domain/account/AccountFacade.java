package me.korzeniak.myportal.finance.domain.account;

import me.korzeniak.myportal.finance.domain.account.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AccountFacade {

    private final AccountProvider accountProvider;

    public HierarchyDto getAccounts(AccountType type) {
        return accountProvider.getAccountHierarchy(type);
    }

    //OTHER
    public Optional<AccountDto> getAccount(UUID id, AccountType accountType) {
        return accountProvider.getAccount(id, accountType);
    }

    public GroupDto addAccountGroup(GroupDto group) {
        return accountProvider.addGroup(group);
    }

    public Optional<GroupDto> editAccountGroup(GroupSimpleDto group) {
        return accountProvider.editGroup(group);
    }

    public AccountDto addAccount(UUID groupId, AccountSimpleDto account) {
        return accountProvider.addAccount(groupId, account);
    }

    public Optional<AccountDto> editAccount(AccountSimpleDto account) {
        return accountProvider.editAccount(account);
    }

    public AccountDto moveAccount(AccountOperationDto operationDto) {
        return accountProvider.moveAccount(operationDto);
    }
}
