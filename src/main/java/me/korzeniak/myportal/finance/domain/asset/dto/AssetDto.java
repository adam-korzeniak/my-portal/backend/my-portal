package me.korzeniak.myportal.finance.domain.asset.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;


@Data
public class AssetDto {
    private UUID id;
    private String name;
    private BigDecimal amount = BigDecimal.ZERO;

    public void addAmount(BigDecimal amount) {
        this.amount = this.amount.add(amount);
    }

    public void subtractAmount(BigDecimal amount) {
        this.amount = this.amount.subtract(amount);
    }
}
