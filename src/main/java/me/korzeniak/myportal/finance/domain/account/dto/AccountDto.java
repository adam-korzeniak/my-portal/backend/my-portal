package me.korzeniak.myportal.finance.domain.account.dto;

import me.korzeniak.myportal.finance.shared.Default;
import lombok.Getter;

import java.util.UUID;

@Getter
public class AccountDto {
    private final UUID id;
    private final String name;
    private final AccountType type;
    private final Integer order;
    private final GroupSimpleDto group;

    public AccountDto(String name, AccountType type, GroupSimpleDto group) {
        this(UUID.randomUUID(), name, type, group, null);
    }

    @Default
    public AccountDto(UUID id, String name, AccountType type, GroupSimpleDto group, Integer order) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.order = order;
        this.group = group;
    }
}
