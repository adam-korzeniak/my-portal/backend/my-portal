package me.korzeniak.myportal.finance.domain.asset;

import me.korzeniak.myportal.finance.domain.account.AccountFacade;
import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import me.korzeniak.myportal.finance.domain.account.dto.HierarchyDto;
import me.korzeniak.myportal.finance.domain.asset.dto.AssetDto;
import me.korzeniak.myportal.finance.domain.asset.dto.AssetHierarchyDto;
import me.korzeniak.myportal.finance.domain.transaction.TransactionFacade;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AssetFacade {
    private final AccountFacade accountFacade;
    private final TransactionFacade transactionFacade;
    private final AssetMapper assetMapper;

    public AssetHierarchyDto getAllAssets() {
        HierarchyDto assetHierarchy = accountFacade.getAccounts(AccountType.ASSET);
        List<TransactionDto> allTransactions = transactionFacade.getAllTransactions();
        Map<UUID, AssetDto> assets = calculateAssets(assetHierarchy, allTransactions);

        return new AssetHierarchyDto(assetHierarchy, assets);
    }

    private Map<UUID, AssetDto> calculateAssets(HierarchyDto assetHierarchy, List<TransactionDto> allTransactions) {
        Map<UUID, AssetDto> assets = assetHierarchy.getAllAccounts().stream()
                .map(assetMapper::map)
                .collect(Collectors.toMap(AssetDto::getId, Function.identity()));

        allTransactions
//                .stream().filter(t -> t.getSource().getName().equals("ING Główne") || t.getTarget().getName().equals("ING Główne"))
                .forEach(transaction -> applyTransaction(transaction, assets));

        return assets;
    }

    //TODO: Do jakiejś wyższej warstwy

    private void applyTransaction(TransactionDto transaction, Map<UUID, AssetDto> assets) {
        switch (transaction.getType()) {
            case INCOME, INITIAL -> processIncome(transaction, assets);
            case EXPENSE -> processExpense(transaction, assets);
            case TRANSFER -> processTransfer(transaction, assets);
        }
    }

    private void processIncome(TransactionDto transaction, Map<UUID, AssetDto> assets) {
        UUID assetId = transaction.getTarget().getId();
        assets.get(assetId).addAmount(transaction.getAmount());
    }

    private void processExpense(TransactionDto transaction, Map<UUID, AssetDto> assets) {
        UUID assetId = transaction.getSource().getId();
        assets.get(assetId).subtractAmount(transaction.getAmount());
    }

    private void processTransfer(TransactionDto transaction, Map<UUID, AssetDto> assets) {
        UUID sourceAssetId = transaction.getSource().getId();
        UUID targetAssetId = transaction.getTarget().getId();
        assets.get(sourceAssetId).subtractAmount(transaction.getAmount());
        assets.get(targetAssetId).addAmount(transaction.getAmount());
    }
}
