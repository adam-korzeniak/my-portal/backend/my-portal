package me.korzeniak.myportal.finance.domain.account.dto;

import me.korzeniak.myportal.finance.shared.Default;
import lombok.Getter;

import java.util.UUID;

@Getter
public class GroupSimpleDto {
    private final UUID id;
    private final String name;
    private final AccountType type;
    private final Integer order;

    public GroupSimpleDto(String name, AccountType type) {
        this(UUID.randomUUID(), name, type, null);
    }

    @Default
    public GroupSimpleDto(UUID id, String name, AccountType type, Integer order) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.order = order;
    }
}
