package me.korzeniak.myportal.finance.domain.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class AccountOperationDto {
    private UUID accountId;
    private UUID groupId;
    private AccountType accountType;
}
