package me.korzeniak.myportal.finance.domain.transaction.dto;

public enum TransactionType {
    INCOME, EXPENSE, TRANSFER, INITIAL
}
