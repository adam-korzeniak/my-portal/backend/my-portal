package me.korzeniak.myportal.finance.domain.transaction;

import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TransactionProvider {
    TransactionDto createTransaction(TransactionDto transaction);

    Optional<TransactionDto> editTransaction(TransactionDto transaction);

    List<TransactionDto> getAllTransactions();

    List<TransactionDto> getTransactions(int limit);

    void deleteTransaction(UUID id);
}
