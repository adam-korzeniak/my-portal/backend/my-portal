package me.korzeniak.myportal.finance.domain.transaction;

import me.korzeniak.myportal.finance.domain.account.dto.AccountDto;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionSimpleDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface TransactionMapper {

    @Mapping(target = "id", source = "transaction.id")
    @Mapping(target = "type", source = "transaction.type")
    TransactionDto mapToExpandedDto(TransactionSimpleDto transaction, AccountDto source, AccountDto target);
}
