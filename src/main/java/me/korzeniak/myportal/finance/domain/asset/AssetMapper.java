package me.korzeniak.myportal.finance.domain.asset;

import me.korzeniak.myportal.finance.domain.account.dto.AccountSimpleDto;
import me.korzeniak.myportal.finance.domain.asset.dto.AssetDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface AssetMapper {

    @Mapping(target = "amount", ignore = true)
    AssetDto map(AccountSimpleDto asset);
}
