package me.korzeniak.myportal.finance.domain.account;

import me.korzeniak.myportal.finance.domain.account.dto.*;

import java.util.Optional;
import java.util.UUID;

public interface AccountProvider {
    HierarchyDto getAccountHierarchy(AccountType type);

    Optional<AccountDto> getAccount(UUID id, AccountType type);

    GroupDto addGroup(GroupDto group);

    Optional<GroupDto> editGroup(GroupSimpleDto group);

    AccountDto addAccount(UUID groupId, AccountSimpleDto account);

    Optional<AccountDto> editAccount(AccountSimpleDto account);

    AccountDto moveAccount(AccountOperationDto operationDto);
}
