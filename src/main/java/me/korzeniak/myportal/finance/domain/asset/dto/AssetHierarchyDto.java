package me.korzeniak.myportal.finance.domain.asset.dto;

import me.korzeniak.myportal.finance.domain.account.dto.AccountSimpleDto;
import me.korzeniak.myportal.finance.domain.account.dto.GroupDto;
import me.korzeniak.myportal.finance.domain.account.dto.HierarchyDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Data
public class AssetHierarchyDto {
    private BigDecimal total = BigDecimal.ZERO;
    private List<AssetGroupDto> groups = new ArrayList<>();

    public AssetHierarchyDto(HierarchyDto accountHierarchy, Map<UUID, AssetDto> assets) {
        for (GroupDto accountGroup : accountHierarchy.getGroups()) {
            AssetGroupDto assetGroup = new AssetGroupDto(accountGroup.getId(), accountGroup.getName());
            for (AccountSimpleDto account : accountGroup.getAccounts()) {
                AssetDto asset = assets.get(account.getId());
                assetGroup.addAsset(asset);
                assetGroup.addAmount(asset.getAmount());
                this.addAmount(asset.getAmount());
            }
            groups.add(assetGroup);
        }
    }

    private void addAmount(BigDecimal amount) {
        this.total = this.total.add(amount);
    }
}
