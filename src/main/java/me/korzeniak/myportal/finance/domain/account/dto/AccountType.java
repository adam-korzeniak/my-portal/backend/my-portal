package me.korzeniak.myportal.finance.domain.account.dto;

public enum AccountType {
    ASSET, INCOME, EXPENSE
}
