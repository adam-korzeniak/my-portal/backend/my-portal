package me.korzeniak.myportal.finance.domain.transaction.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Data
public class TransactionSearchDto {
    private String id;
    private Set<TransactionType> type;
    private Set<UUID> source;
    private Set<UUID> target;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private LocalDateTime minDate;
    private LocalDateTime maxDate;
    private String comment;
    private String shop;
}
