package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request;

import lombok.Data;

@Data
public class GroupRequest {
    private String name;
}
