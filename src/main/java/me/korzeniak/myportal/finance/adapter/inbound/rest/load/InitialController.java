package me.korzeniak.myportal.finance.adapter.inbound.rest.load;

import me.korzeniak.myportal.finance.adapter.inbound.file.initial_load.FileReader;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/trigger")
class InitialController {

    private final FileReader fileReader;

    @GetMapping
    void getAccountsHierarchy() {
        fileReader.loadData();
    }


}
