package me.korzeniak.myportal.finance.adapter.outbound.db.account;

import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountEntity;
import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountGroupEntity;
import me.korzeniak.myportal.finance.domain.account.dto.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface AccountDbMapper {

    default List<AccountGroupEntity> mapAccountGroupsToEntity(HierarchyDto accounts) {
        return mapAccountGroupsToEntity(accounts.getGroups());
    }

    List<AccountGroupEntity> mapAccountGroupsToEntity(List<GroupDto> groups);

    @Mapping(target = "uuid", source = "id")
    AccountGroupEntity mapToEntity(GroupDto group);

    @Mapping(target = "uuid", source = "id")
    @Mapping(target = "accounts", ignore = true)
    AccountGroupEntity mapToEntity(GroupSimpleDto group);

    @Mapping(target = "uuid", source = "id")
    AccountEntity mapAccountToEntity(AccountDto account);

    default HierarchyDto mapToDtoAccountGroups(List<AccountGroupEntity> groups) {
        return new HierarchyDto(mapAccountGroupsToDto(groups));
    }

    List<GroupDto> mapAccountGroupsToDto(List<AccountGroupEntity> groups);

    @Mapping(target = "id", source = "uuid")
    @Mapping(target = "accounts", source = "accounts")
    GroupDto mapToDto(AccountGroupEntity group);

    @Mapping(target = "id", source = "uuid")
    AccountDto mapToDto(AccountEntity account);

    @Mapping(target = "id", source = "uuid")
    AccountSimpleDto mapToSimpleDto(AccountEntity account);

    @Mapping(target = "id", source = "uuid")
    GroupSimpleDto mapToSimpleDto(AccountGroupEntity group);

    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "accounts", ignore = true)
    AccountGroupEntity update(@MappingTarget AccountGroupEntity entity, GroupSimpleDto group);

    @Mapping(target = "uuid", source = "account.id")
    @Mapping(target = "name", source = "account.name")
    @Mapping(target = "type", source = "account.type")
    @Mapping(target = "order", source = "account.order")
    @Mapping(target = "group", source = "group")
    AccountEntity mapToEntity(AccountSimpleDto account, AccountGroupEntity group);

    @Mapping(target = "uuid", source = "id")
    @Mapping(target = "group", ignore = true)
    AccountEntity mapAccountToEntity(AccountSimpleDto account);


    @Mapping(target = "uuid", source = "account.id")
    @Mapping(target = "group", ignore = true)
    AccountEntity update(@MappingTarget AccountEntity entity, AccountSimpleDto account);
}
