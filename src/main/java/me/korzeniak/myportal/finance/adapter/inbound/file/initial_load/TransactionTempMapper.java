package me.korzeniak.myportal.finance.adapter.inbound.file.initial_load;

import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionSimpleDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface TransactionTempMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "comment", ignore = true)
    @Mapping(target = "shop", ignore = true)
    TransactionSimpleDto map(Transaction group, UUID sourceAccountId, UUID targetAccountId );

    default LocalDateTime mapDate(LocalDate date) {
        return date.atStartOfDay();
    }
}
