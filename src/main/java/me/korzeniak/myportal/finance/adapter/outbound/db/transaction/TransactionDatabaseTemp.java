package me.korzeniak.myportal.finance.adapter.outbound.db.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class TransactionDatabaseTemp  {

    private final TransactionRepository transactionRepository;

    public void removeAll() {
        transactionRepository.deleteAll();
    }
}
