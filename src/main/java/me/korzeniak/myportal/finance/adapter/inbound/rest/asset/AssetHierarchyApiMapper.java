package me.korzeniak.myportal.finance.adapter.inbound.rest.asset;

import me.korzeniak.myportal.finance.adapter.inbound.rest.asset.dto.AssetHierarchyResponse;
import me.korzeniak.myportal.finance.domain.asset.dto.AssetHierarchyDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface AssetHierarchyApiMapper {
    AssetHierarchyResponse map(AssetHierarchyDto assets);
}
