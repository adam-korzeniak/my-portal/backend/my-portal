package me.korzeniak.myportal.finance.adapter.inbound.rest.transaction;

import me.korzeniak.myportal.finance.adapter.inbound.rest.transaction.protocol.TransactionRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.transaction.protocol.TransactionResponse;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionSimpleDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface TransactionApiMapper {


    @Mapping(target = "id", ignore = true)
    @Mapping(target = "sourceAccountId", source = "sourceId")
    @Mapping(target = "targetAccountId", source = "targetId")
    @Mapping(target = "amount", source = "amount", qualifiedByName = "round")
    TransactionSimpleDto map(TransactionRequest transaction);

    TransactionResponse map(TransactionDto transaction);

    List<TransactionResponse> map(List<TransactionDto> transactions);

    @Named("round")
    default BigDecimal round(BigDecimal value) {
        return value.setScale(2, RoundingMode.DOWN);
    }

}
