package me.korzeniak.myportal.finance.adapter.inbound.rest.asset;

import me.korzeniak.myportal.finance.adapter.inbound.rest.asset.dto.AssetHierarchyResponse;
import me.korzeniak.myportal.finance.domain.asset.AssetFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/asset")
class AssetController {

    private final AssetFacade assetFacade;
    private final AssetHierarchyApiMapper mapper;

    @GetMapping
    AssetHierarchyResponse getAssets() {
        return mapper.map(assetFacade.getAllAssets());
    }


}
