package me.korzeniak.myportal.finance.adapter.outbound.db.transaction.model;

import me.korzeniak.myportal.finance.adapter.outbound.db.BaseEntity;
import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountEntity;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionType;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "transaction")
public class TransactionEntity extends BaseEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_id_sequence")
    @SequenceGenerator(name = "transaction_id_sequence", sequenceName = "transaction_id_sequence")
    @Setter(AccessLevel.NONE)
    private Long internalId;

    private UUID uuid;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @ManyToOne(optional = false)
    @JoinColumn(name = "source_id", nullable = false)
    private AccountEntity source;

    @ManyToOne(optional = false)
    @JoinColumn(name = "target_id", nullable = false)
    private AccountEntity target;

    private BigDecimal amount;
    private LocalDateTime date;
    private String comment;
    private String shop;
}
