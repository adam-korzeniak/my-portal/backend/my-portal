package me.korzeniak.myportal.finance.adapter.inbound.rest.transaction;

import me.korzeniak.myportal.finance.adapter.inbound.rest.transaction.protocol.TransactionRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.transaction.protocol.TransactionResponse;
import me.korzeniak.myportal.finance.domain.transaction.TransactionFacade;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/transaction")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionFacade transactionFacade;
    private final TransactionApiMapper mapper;

    @PostMapping
    public TransactionResponse createTransaction(@Valid @RequestBody TransactionRequest transaction) {
        TransactionDto persisted = transactionFacade.createTransaction(mapper.map(transaction));
        return mapper.map(persisted);
    }

    @GetMapping
    public List<TransactionResponse> getTransactions() {
        List<TransactionDto> persisted = transactionFacade.getTransactions(10);
        return mapper.map(persisted);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteTransaction(@PathVariable UUID id) {
        transactionFacade.deleteTransaction(id);
    }
}
