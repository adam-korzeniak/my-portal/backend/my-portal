package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class GroupResponse extends GroupSimpleResponse {
    private List<AccountSimpleResponse> accounts;
}
