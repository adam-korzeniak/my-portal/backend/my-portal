package me.korzeniak.myportal.finance.adapter.inbound.rest.asset.dto;

import me.korzeniak.myportal.finance.shared.PriceUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Data
public class AssetHierarchyResponse {
    //    @JsonFormat(shape = JsonFormat.Shape.STRING)
//    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal total;
    private List<AssetGroupResponse> groups;

    public void setTotal(BigDecimal total) {
        this.total = PriceUtils.round(total);
    }

    @Data
    public static class AssetGroupResponse {
        private UUID id;
        private String name;
        private BigDecimal amount;
        private List<AssetResponse> assets;

        public void setAmount(BigDecimal amount) {
            this.amount = PriceUtils.round(amount);
        }

    }

    @Data
    public static class AssetResponse {
        private UUID id;
        private String name;
        private BigDecimal amount;

        public void setAmount(BigDecimal amount) {
            this.amount = PriceUtils.round(amount);
        }

    }
}
