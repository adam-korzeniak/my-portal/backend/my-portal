package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request;

import me.korzeniak.myportal.finance.domain.account.dto.Operation;
import lombok.Data;

import java.util.UUID;

@Data
public class OperationRequest {
    private UUID groupId;
    private UUID accountId;
    private Operation operation;
}
