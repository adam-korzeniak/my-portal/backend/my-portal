package me.korzeniak.myportal.finance.adapter.outbound.db.transaction;

import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import me.korzeniak.myportal.finance.adapter.outbound.db.account.AccountRepository;
import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountEntity;
import me.korzeniak.myportal.finance.adapter.outbound.db.transaction.model.TransactionEntity;
import me.korzeniak.myportal.finance.domain.transaction.TransactionProvider;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class TransactionDatabaseProvider implements TransactionProvider {

    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;
    private final TransactionDbMapper mapper;
    private final EntityManager em;

    @Override
    public TransactionDto createTransaction(TransactionDto transaction) {
//TODO:
        //        AccountEntity source = em.getReference(AccountEntity.class, transaction.getSource().getId());
//        AccountEntity target = em.getReference(AccountEntity.class, transaction.getTarget().getId());
        AccountEntity source = accountRepository.findByUuid(transaction.getSource().getId()).orElseThrow();
        AccountEntity target = accountRepository.findByUuid(transaction.getTarget().getId()).orElseThrow();
        TransactionEntity entity = mapper.map(transaction, source, target, UUID.randomUUID());
        transactionRepository.save(entity);
        return mapper.map(entity);
    }

    @Override
    public Optional<TransactionDto> editTransaction(TransactionDto transaction) {
        return Optional.empty();
    }

    @Override
    public List<TransactionDto> getAllTransactions() {
        return mapper.map(transactionRepository.findAll());
    }

    @Override
    public List<TransactionDto> getTransactions(int limit) {
        Sort sort = Sort.by("date").descending();
        return mapper.map(transactionRepository.findAll(PageRequest.of(0,20, sort)));
    }

    @Override
    public void deleteTransaction(UUID id) {
        transactionRepository.deleteByUuid(id);
    }
}
