package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response;

import lombok.Data;

import java.util.List;

@Data
public class HierarchyResponse {
    private List<GroupResponse> groups;

}
