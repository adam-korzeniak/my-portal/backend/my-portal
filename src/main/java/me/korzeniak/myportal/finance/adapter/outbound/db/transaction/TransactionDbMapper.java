package me.korzeniak.myportal.finance.adapter.outbound.db.transaction;


import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountEntity;
import me.korzeniak.myportal.finance.adapter.outbound.db.transaction.model.TransactionEntity;
import me.korzeniak.myportal.finance.domain.account.dto.AccountSimpleDto;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface TransactionDbMapper {


    @Mapping(target = "uuid", source = "uuid")
    @Mapping(target = "type", source = "transaction.type")
    @Mapping(target = "source", source = "source")
    @Mapping(target = "target", source = "target")
    TransactionEntity map(TransactionDto transaction, AccountEntity source, AccountEntity target, UUID uuid);

    List<TransactionDto> map(List<TransactionEntity> transactions);

    List<TransactionDto> map( Page<TransactionEntity> transactions);

    @Mapping(target = "id", source = "uuid")
    TransactionDto map(TransactionEntity transaction);

    @Mapping(target = "id", source = "uuid")
    AccountSimpleDto mapToSimpleDto(AccountEntity account);


}
