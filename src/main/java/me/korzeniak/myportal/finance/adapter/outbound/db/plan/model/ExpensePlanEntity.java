package me.korzeniak.myportal.finance.adapter.outbound.db.plan.model;

import me.korzeniak.myportal.finance.adapter.outbound.db.BaseEntity;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
//@Entity
//@Table(name = "expense_plan")
public class ExpensePlanEntity extends BaseEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "plan_id_sequence")
    @SequenceGenerator(name = "plan_id_sequence", sequenceName = "plan_id_sequence")
    @Setter(AccessLevel.NONE)
    private Long internalId;

    private UUID uuid;

    private BigDecimal expectedIncome;
    private List<BigDecimal> expectedExpenses;
    private Month date;
    private String comment;

    private BigDecimal startingMonthValue;
    private BigDecimal longTermExpectedStartingValue;
    private BigDecimal longTermExpectedEndingValue;

    //TODO: Rozdzielić
    // Osobno wpisy na plan początkowy na cały rok
    // Osobno wpisy na plan zmodyfikowany na cały rok

    // Osobno wpisy na plan comiesięczny, które uwzględniają tylko aktualny miesiąc
}
