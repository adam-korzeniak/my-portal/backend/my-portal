package me.korzeniak.myportal.finance.adapter.inbound.rest.account;

import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request.AccountRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request.GroupRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request.OperationRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response.AccountResponse;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response.GroupResponse;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response.HierarchyResponse;
import me.korzeniak.myportal.finance.domain.account.AccountFacade;
import me.korzeniak.myportal.finance.domain.account.dto.*;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/account")
class AccountController {

    private final AccountFacade accountFacade;
    private final AccountApiMapper accountApiMapper;

    @GetMapping("/{accountType}")
    HierarchyResponse getAccountsHierarchy(@PathVariable AccountType accountType) {
        return accountApiMapper.mapToResponse(accountFacade.getAccounts(accountType));
    }

    @PostMapping("/{accountType}/group")
    GroupResponse createAccountGroup(@PathVariable AccountType accountType, @Valid @RequestBody GroupRequest group) {
        GroupDto dto = accountApiMapper.mapToDto(group, accountType);
        return accountApiMapper.mapToResponse(accountFacade.addAccountGroup(dto));
    }

    @PutMapping("/{accountType}/group/{groupId}")
    GroupResponse editAccountGroup(@PathVariable AccountType accountType, @PathVariable String groupId,
                                   @Valid @RequestBody GroupRequest group) {
        GroupSimpleDto dto = accountApiMapper.mapToDto(group, accountType, groupId);
        return accountFacade.editAccountGroup(dto)
                .map(accountApiMapper::mapToResponse)
                .orElse(null);
    }

    @PostMapping("/{accountType}/group/{groupId}/account")
    AccountResponse addAccount(@PathVariable AccountType accountType, @PathVariable String groupId,
                                                                                                        @Valid @RequestBody AccountRequest account) {
        AccountSimpleDto dto = accountApiMapper.mapToDto(account, accountType);
        AccountDto result = accountFacade.addAccount(UUID.fromString(groupId), dto);
        return accountApiMapper.mapToResponse(result);
    }

    @PutMapping("/{accountType}/account/{accountId}")
    AccountResponse editAccount(@PathVariable AccountType accountType, @PathVariable String accountId,
                                                                                                         @Valid @RequestBody AccountRequest account) {
        AccountSimpleDto dto = accountApiMapper.mapToDto(account, accountType, accountId);
        return accountFacade.editAccount(dto)
                .map(accountApiMapper::mapToResponse)
                .orElse(null);
    }

    @PatchMapping("/{accountType}")
    AccountResponse modifyAccount(@PathVariable AccountType accountType, @Valid @RequestBody OperationRequest operationRequest) {
        switch (operationRequest.getOperation()) {
            case MOVE -> {
                return moveAccount(accountType, operationRequest);
            }
            default -> throw new NotImplementedException();
        }
    }

    private AccountResponse moveAccount(AccountType accountType, OperationRequest operationRequest) {
        AccountOperationDto dto = accountApiMapper.mapToDto(operationRequest, accountType);
        AccountDto result = accountFacade.moveAccount(dto);
        return accountApiMapper.mapToResponse(result);
    }


}
