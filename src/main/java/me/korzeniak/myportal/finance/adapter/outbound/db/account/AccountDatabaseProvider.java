package me.korzeniak.myportal.finance.adapter.outbound.db.account;

import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountEntity;
import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountGroupEntity;
import me.korzeniak.myportal.finance.domain.account.AccountProvider;
import me.korzeniak.myportal.finance.domain.account.dto.*;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
class AccountDatabaseProvider implements AccountProvider {
    private final GroupRepository groupRepository;
    private final AccountRepository accountRepository;
    private final AccountDbMapper accountMapper;

    public HierarchyDto getAccountHierarchy(AccountType type) {
        List<AccountGroupEntity> groups = groupRepository.findByType(type);
        return accountMapper.mapToDtoAccountGroups(groups);
    }


    @Override
    public Optional<AccountDto> getAccount(UUID id, AccountType type) {
        return accountRepository.findByUuidAndType(id, type)
                .map(accountMapper::mapToDto);
    }

    @Override
    public GroupDto addGroup(GroupDto group) {
        AccountGroupEntity persisted = groupRepository.save(accountMapper.mapToEntity(group));
        return accountMapper.mapToDto(persisted);
    }

    @Override
    @Transactional
    public Optional<GroupDto> editGroup(GroupSimpleDto group) {
        return groupRepository.findByUuidAndType(group.getId(), group.getType())
                .map(entity -> accountMapper.update(entity, group))
                .map(accountMapper::mapToDto);
    }

    @Override
    @Transactional
    public AccountDto addAccount(UUID groupId, AccountSimpleDto account) {
        AccountGroupEntity groupEntity = groupRepository.findByUuidAndType(groupId, account.getType())
                .orElseThrow();
        AccountEntity accountEntity = accountRepository.save(accountMapper.mapToEntity(account, groupEntity));
        return accountMapper.mapToDto(accountEntity);
    }

    @Override
    @Transactional
    public Optional<AccountDto> editAccount(AccountSimpleDto account) {
        return accountRepository.findByUuidAndType(account.getId(), account.getType())
                .map(entity -> accountMapper.update(entity, account))
                .map(accountMapper::mapToDto);
    }

    @Override
    @Transactional
    public AccountDto moveAccount(AccountOperationDto operationDto) {
        AccountEntity accountEntity = accountRepository.findByUuidAndType(operationDto.getAccountId(), operationDto.getAccountType())
                .orElseThrow();
        AccountGroupEntity targetGroupEntity = groupRepository.findByUuidAndType(operationDto.getGroupId(), operationDto.getAccountType())
                .orElseThrow();
        accountEntity.replaceGroup(targetGroupEntity);
        return accountMapper.mapToDto(accountEntity);
    }

}
