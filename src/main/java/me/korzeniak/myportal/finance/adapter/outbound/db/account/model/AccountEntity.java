package me.korzeniak.myportal.finance.adapter.outbound.db.account.model;

import me.korzeniak.myportal.finance.adapter.outbound.db.BaseEntity;
import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "account")
public class AccountEntity extends BaseEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_id_sequence")
    @SequenceGenerator(name = "account_id_sequence", sequenceName = "account_id_sequence")
    @Setter(AccessLevel.NONE)
    private Long internalId;

    private UUID uuid;
    private String name;
    private Integer order;
    @Enumerated(EnumType.STRING)
    private AccountType type;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    private AccountGroupEntity group;

    public void replaceGroup(AccountGroupEntity targetGroup) {
        this.group.removeAccount(this);
        targetGroup.addAccount(this);
        this.group = targetGroup;
    }

}
