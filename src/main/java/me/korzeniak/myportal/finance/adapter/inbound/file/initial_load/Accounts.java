package me.korzeniak.myportal.finance.adapter.inbound.file.initial_load;

import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class Accounts {

    private Map<String, AccountGroup> groups = new HashMap<>();
    private Map<String, Account> accounts = new HashMap<>();

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AccountGroup {
        private String name;
        private AccountType type;
        private UUID id;
        private List<Account> accounts = new ArrayList<>();

        public AccountGroup(String name, AccountType type) {
            this.name = name;
            this.type = type;
        }

        public void addAccount(Account account) {
            accounts.add(account);
        }
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Account {
        private String name;
        private AccountType type;
        private UUID id;
        private UUID groupId;

        public Account(String name, AccountType type) {
            this.name = name;
            this.type = type;
        }
    }

    public AccountGroup getGroup(String name, AccountType type) {
        return groups.get(name + type);
    }

    public Account getAccount(String name, AccountType type) {
        return accounts.get(name + type);
    }
}
