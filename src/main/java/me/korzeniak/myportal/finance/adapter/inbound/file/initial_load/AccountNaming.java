package me.korzeniak.myportal.finance.adapter.inbound.file.initial_load;

import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class AccountNaming {
    private String groupName;
    private String accountName;
    private AccountType type;

    public AccountNaming(String accountName, AccountType type) {
        this.groupName = "XXX";
        this.accountName = accountName;
        this.type = type;
    }

    public String getComplexName() {
        return groupName + ":" + accountName;
    }
}
