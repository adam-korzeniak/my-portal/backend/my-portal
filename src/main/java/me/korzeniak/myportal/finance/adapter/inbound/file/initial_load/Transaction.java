package me.korzeniak.myportal.finance.adapter.inbound.file.initial_load;

import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionType;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDate;

@Slf4j
@Data
class Transaction {

    private TransactionType type;
    private String source;
    private String target;
    private BigDecimal amount;
    private LocalDate date;

    public Transaction(String target, String amount) {
        this.type = TransactionType.INITIAL;
        this.source = target;
        this.target = target;
        this.amount = new BigDecimal(amount);
        this.date = LocalDate.of(2022, 1, 1);
    }

    public Transaction(TransactionType type, String source, String target, String amount, LocalDate date) {
        this.type = type;
        this.source = source;
        this.target = target;
        try {

        this.amount = new BigDecimal(amount);
        } catch (NumberFormatException ex) {
            log.error("Error occurred", ex);

        }
        this.date = date;
    }

    public AccountType getSourceAccountType() {
        return switch (type) {
            case INCOME -> AccountType.INCOME;
            case EXPENSE, TRANSFER, INITIAL -> AccountType.ASSET;
        };

    }

    public AccountType getTargetAccountType() {
        return switch (type) {
            case EXPENSE -> AccountType.EXPENSE;
            case INCOME, TRANSFER, INITIAL -> AccountType.ASSET;
        };
    }
}
