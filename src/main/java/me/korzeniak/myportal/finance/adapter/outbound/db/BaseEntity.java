package me.korzeniak.myportal.finance.adapter.outbound.db;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;

public class BaseEntity {
    @CreatedDate
    private LocalDateTime creationDate;
    @LastModifiedDate
    private LocalDateTime modificationDate;
    @CreatedBy
    private LocalDateTime createdBy;
    @LastModifiedBy
    private LocalDateTime modifiedBy;
}
