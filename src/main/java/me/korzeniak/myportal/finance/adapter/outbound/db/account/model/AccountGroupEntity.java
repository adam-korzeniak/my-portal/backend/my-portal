package me.korzeniak.myportal.finance.adapter.outbound.db.account.model;

import me.korzeniak.myportal.finance.adapter.outbound.db.BaseEntity;
import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "account_group")
public class AccountGroupEntity extends BaseEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_group_id_sequence")
    @SequenceGenerator(name = "account_group_id_sequence", sequenceName = "account_group_id_sequence")
    @Setter(AccessLevel.NONE)
    private Long internalId;

    private UUID uuid;
    private String name;
    private Integer order;
    @Enumerated(EnumType.STRING)
    private AccountType type;

    @OneToMany(mappedBy = "group", cascade = CascadeType.PERSIST)
    private List<AccountEntity> accounts;

    public void addAccount(AccountEntity account) {
        accounts.add(account);
    }

    public void removeAccount(AccountEntity account) {
        accounts.remove(account);
    }

}
