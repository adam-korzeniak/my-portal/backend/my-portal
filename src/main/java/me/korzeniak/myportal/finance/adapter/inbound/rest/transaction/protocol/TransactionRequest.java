package me.korzeniak.myportal.finance.adapter.inbound.rest.transaction.protocol;

import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class TransactionRequest {
    private TransactionType type;
    private UUID sourceId;
    private UUID targetId;
    private BigDecimal amount;
    private LocalDateTime date;
    private String comment;
    private String shop;
}
