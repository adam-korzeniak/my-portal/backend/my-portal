package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AccountResponse extends AccountSimpleResponse {
    private GroupSimpleResponse group;
}
