package me.korzeniak.myportal.finance.adapter.outbound.db.transaction;

import me.korzeniak.myportal.finance.adapter.outbound.db.transaction.model.TransactionEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

interface TransactionRepository extends JpaRepository<TransactionEntity, Long> {

    @Transactional
    void deleteByUuid(UUID id);
}
