package me.korzeniak.myportal.finance.adapter.inbound.rest.transaction.protocol;

import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import lombok.Data;

import java.util.UUID;

@Data
public class AccountResponse {
    private final UUID id;
    private final String name;
    private final AccountType type;
}