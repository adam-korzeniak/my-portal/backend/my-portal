package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request;

import lombok.Data;

@Data
public class AccountRequest {
    private String name;
}
