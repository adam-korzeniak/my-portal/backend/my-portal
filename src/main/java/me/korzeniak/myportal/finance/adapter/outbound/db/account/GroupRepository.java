package me.korzeniak.myportal.finance.adapter.outbound.db.account;

import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountGroupEntity;
import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

interface GroupRepository extends JpaRepository<AccountGroupEntity, Long> {

    List<AccountGroupEntity> findByType(AccountType type);

    Optional<AccountGroupEntity> findByUuidAndType(UUID id, AccountType type);
}
