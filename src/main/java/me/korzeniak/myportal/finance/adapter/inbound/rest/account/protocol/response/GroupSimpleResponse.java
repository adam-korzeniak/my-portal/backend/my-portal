package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response;

import lombok.Data;

@Data
public class GroupSimpleResponse {
    private String id;
    private String name;
    private Integer order;
}
