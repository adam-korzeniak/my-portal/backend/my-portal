package me.korzeniak.myportal.finance.adapter.inbound.rest.account;

import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request.AccountRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request.GroupRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.request.OperationRequest;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response.GroupResponse;
import me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response.HierarchyResponse;
import me.korzeniak.myportal.finance.domain.account.dto.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.UUID;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
interface AccountApiMapper {

    HierarchyResponse mapToResponse(HierarchyDto accounts);

    GroupResponse mapToResponse(GroupDto group);

    //TODO:
    default GroupDto mapToDto(GroupRequest group, AccountType type) {
        return new GroupDto(group.getName(), type);
    }

    //TODO:
    default GroupSimpleDto mapToDto(GroupRequest group, AccountType type, String id) {
        return new GroupSimpleDto(UUID.fromString(id), group.getName(), type, null);
    }

    default AccountSimpleDto mapToDto(AccountRequest account, AccountType type) {
        return new AccountSimpleDto(account.getName(), type);
    }

    @Mapping(target = "order", ignore = true)
    AccountSimpleDto mapToDto(AccountRequest account, AccountType type, String id);

    me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response.AccountResponse mapToResponse(AccountDto result);

    @Mapping(target = "accountId", source = "operationRequest.accountId")
    @Mapping(target = "groupId", source = "operationRequest.groupId")
    AccountOperationDto mapToDto(OperationRequest operationRequest, AccountType accountType);
}
