package me.korzeniak.myportal.finance.adapter.outbound.db.account;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class AccountDatabaseProviderTemp {
    private final GroupRepository groupRepository;
    private final AccountRepository accountRepository;

    public void removeAll() {
        accountRepository.deleteAll();
        groupRepository.deleteAll();
    }


}
