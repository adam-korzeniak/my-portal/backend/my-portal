package me.korzeniak.myportal.finance.adapter.inbound.file.initial_load;

import me.korzeniak.myportal.finance.adapter.outbound.db.account.AccountDatabaseProviderTemp;
import me.korzeniak.myportal.finance.adapter.outbound.db.transaction.TransactionDatabaseTemp;
import me.korzeniak.myportal.finance.domain.account.AccountFacade;
import me.korzeniak.myportal.finance.domain.account.dto.AccountDto;
import me.korzeniak.myportal.finance.domain.account.dto.AccountSimpleDto;
import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import me.korzeniak.myportal.finance.domain.account.dto.GroupDto;
import me.korzeniak.myportal.finance.domain.transaction.TransactionFacade;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionSimpleDto;
import me.korzeniak.myportal.finance.domain.transaction.dto.TransactionType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class FileReader {

    private final AccountDatabaseProviderTemp accountDatabaseProviderTemp;
    private final TransactionDatabaseTemp transactionDatabaseTemp;
    private final AccountFacade accountFacade;
    private final TransactionFacade transactionFacade;
    private final TransactionTempMapper mapper;

    public static final String DIRECTORY = "src/main/resources/legacy_data/";
    public static final String INITIAL_ASSETS = DIRECTORY + "initial-assets.txt";
    public static final String INCOME_LOCATION = DIRECTORY + "incomes.txt";
    public static final String EXPENSE_LOCATION = DIRECTORY + "expenses.txt";
    public static final String TRANSFERS_LOCATION = DIRECTORY + "transfers.txt";


    public void loadData() {
        List<Transaction> transactions = loadTransactions();
        Accounts accounts = extractAccountsGroups(transactions);

        transactionDatabaseTemp.removeAll();
        accountDatabaseProviderTemp.removeAll();

        for (Accounts.AccountGroup group: accounts.getGroups().values()) {
            GroupDto persisted = accountFacade.addAccountGroup(new GroupDto(group.getName(), group.getType()));
            group.setId(persisted.getId());
            group.getAccounts().forEach(a -> a.setGroupId(persisted.getId()));
        }

        for (Accounts.Account account: accounts.getAccounts().values()) {
            AccountDto persisted = accountFacade.addAccount(account.getGroupId(), new AccountSimpleDto(account.getName(), account.getType()));
            account.setId(persisted.getId());
        }

        for (Transaction transaction: transactions) {
            AccountNaming namingSource = getNaming(transaction.getSource(), transaction.getSourceAccountType());
            Accounts.Account source = accounts.getAccount(namingSource.getComplexName(), namingSource.getType());
            AccountNaming namingTarget = getNaming(transaction.getTarget(), transaction.getTargetAccountType());
            Accounts.Account target = accounts.getAccount(namingTarget.getComplexName(), namingTarget.getType());
            TransactionSimpleDto dto = mapper.map(transaction, source.getId(), target.getId());
            transactionFacade.createTransaction(dto);
        }
    }

    private static Accounts extractAccountsGroups(List<Transaction> transactions) {
        Accounts accounts = new Accounts();
        for (Transaction transaction: transactions) {
            switch (transaction.getType()) {
                case INITIAL -> {
                    AccountNaming asset = getNaming(transaction.getTarget(), AccountType.ASSET);
                    addAccount(accounts, asset);
                }
                case INCOME -> {
                    AccountNaming income = getNaming(transaction.getSource(), AccountType.INCOME);
                    AccountNaming asset = getNaming(transaction.getTarget(), AccountType.ASSET);
                    addAccount(accounts, income);
                    addAccount(accounts, asset);
                }
                case EXPENSE -> {
                    AccountNaming asset = getNaming(transaction.getSource(), AccountType.ASSET);
                    AccountNaming expense = getNaming(transaction.getTarget(), AccountType.EXPENSE);
                    addAccount(accounts, asset);
                    addAccount(accounts, expense);
                }
                case TRANSFER -> {
                    AccountNaming asset1 = getNaming(transaction.getSource(), AccountType.ASSET);
                    AccountNaming asset2 = getNaming(transaction.getTarget(), AccountType.ASSET);
                    addAccount(accounts, asset1);
                    addAccount(accounts, asset2);
                }
            }
        }
        return accounts;
    }

    private static void addAccount(Accounts accounts, AccountNaming naming) {
        Accounts.AccountGroup group = accounts.getGroup(naming.getGroupName(), naming.getType());
        if (group == null) {
            group = new Accounts.AccountGroup(naming.getGroupName(), naming.getType());
            accounts.getGroups().put(naming.getGroupName() + naming.getType(), group);
        }
        if (accounts.getAccount(naming.getComplexName(), naming.getType()) == null) {
            Accounts.Account account = new Accounts.Account(naming.getAccountName(), naming.getType());
            group.addAccount(account);
            accounts.getAccounts().put(naming.getComplexName() + naming.getType(), account);
        }

    }

    private static AccountNaming getNaming(String name, AccountType type) {
        String[] tokens = name.trim().split(":");
        if (tokens.length > 2 || tokens.length < 1) {
            throw new RuntimeException("Name invalid: " + name);
        } else if (tokens.length == 2) {
            return new AccountNaming(tokens[0], tokens[1], type);
        } else {
            return new AccountNaming(tokens[0], type);
        }
    }

    private List<Transaction> loadTransactions() {
        List<Transaction> transactions = new ArrayList<>(500);

        transactions.addAll(getTransactions(INITIAL_ASSETS, this::extractInitialTransactions));
        transactions.addAll(getTransactions(TRANSFERS_LOCATION, this::extractTransferTransactions));
        return transactions;
    }

    List<Transaction> getTransactions(String path, Function<String, List<Transaction>> extractor) {
        try (Stream<String> lines = Files.lines(Paths.get(path))) {
            return lines
                    .map(extractor)
                    .flatMap(Collection::stream)
                    .toList();
        } catch (IOException ex) {
            throw new RuntimeException();
        }
    }

    private List<Transaction> extractInitialTransactions(String line) {
        String[] tokens = line.trim().split("\t");
        if (tokens.length != 2) {
            throw new RuntimeException(line);
        }
        return List.of(new Transaction(tokens[0], tokens[1].replace(",", "")));
    }

    private List<Transaction> extractTransferTransactions(String line) {
        String[] tokens = line.trim().split("\t");

        if (tokens.length < 2) {
            return List.of();
        }

        if (!(tokens.length == 4 || tokens.length == 5 || tokens.length == 8 || tokens.length == 12)) {
            throw new RuntimeException("Length:" + tokens.length);
        }

        List<Transaction> transactions = new ArrayList<>();

        LocalDate month = extractDate(tokens[0]);
        String expenseSource = tokens[1];
        String expenseTarget = tokens[2];
        String expenseAmount = tokens[3].replace(",", "");

        transactions.add(new Transaction(TransactionType.EXPENSE, expenseSource, expenseTarget, expenseAmount, month));

        if (tokens.length == 4 || tokens.length == 5) {
            return transactions;
        }

        String transferSource = tokens[5];
        if (!transferSource.isEmpty()) {
            String transferTarget = tokens[6];
            String transferAmount = tokens[7].replace(",", "");

            transactions.add(new Transaction(TransactionType.TRANSFER, transferSource, transferTarget, transferAmount, month));

        }
        if (tokens.length == 8) {
            return transactions;
        }

        String incomeSource = tokens[9];
        String incomeTarget = tokens[10];
        String incomeAmount = tokens[11].replace(",", "");

        transactions.add(new Transaction(TransactionType.INCOME, incomeSource, incomeTarget, incomeAmount, month));

        return transactions;
    }

    private static LocalDate extractDate(String token) {
        return switch (token) {
            case "Styczeń" -> LocalDate.of(2023, 1, 1);
            case "Luty" -> LocalDate.of(2023, 2, 1);
            case "Marzec" -> LocalDate.of(2023, 3, 1);
            case "Kwiecień" -> LocalDate.of(2023, 4, 1);
            case "Maj" -> LocalDate.of(2023, 5, 1);
            case "Czerwiec" -> LocalDate.of(2023, 6, 1);
            case "Lipiec" -> LocalDate.of(2023, 7, 1);
            case "Sierpień" -> LocalDate.of(2023, 8, 1);
            case "Wrzesień" -> LocalDate.of(2023, 9, 1);
            case "Październik" -> LocalDate.of(2023, 10, 1);
            case "Listopad" -> LocalDate.of(2023, 11, 1);
            case "Grudzień" -> LocalDate.of(2023, 12, 1);
            default -> throw new RuntimeException(token);
        };
    }
}
