package me.korzeniak.myportal.finance.adapter.inbound.rest.account.protocol.response;

import lombok.Data;

@Data
public class AccountSimpleResponse {
    private String id;
    private String name;
    private String type;
    private Integer order;
}
