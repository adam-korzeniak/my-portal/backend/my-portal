package me.korzeniak.myportal.finance.adapter.outbound.db.account;

import me.korzeniak.myportal.finance.adapter.outbound.db.account.model.AccountEntity;
import me.korzeniak.myportal.finance.domain.account.dto.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    Optional<AccountEntity> findByUuid(UUID id);

    Optional<AccountEntity> findByUuidAndType(UUID id, AccountType type);

}
