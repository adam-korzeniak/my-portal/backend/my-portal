package me.korzeniak.myportal;

import org.springframework.boot.SpringApplication;

public class TestMyPortalApplication {

	public static void main(String[] args) {
		SpringApplication.from(MyPortalApplication::main).with(TestcontainersConfiguration.class).run(args);
	}

}
