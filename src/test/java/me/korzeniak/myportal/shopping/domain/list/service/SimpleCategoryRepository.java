package me.korzeniak.myportal.shopping.domain.list.service;

import me.korzeniak.myportal.shopping.domain.list.dto.CategoryQuery;
import me.korzeniak.myportal.shopping.domain.list.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class SimpleCategoryRepository implements CategoryRepository {

    //TODO: Test list concurrency
    private final List<Category> categories = new ArrayList<>();
    private final AtomicLong nextId = new AtomicLong(1);

    @Override
    public Optional<Category> findById(Long id) {
        return categories.stream()
                .filter(element -> id.equals(element.getInternalId()))
                .findFirst()
                .map(this::copy);
    }

    @Override
    public List<Category> findAll() {
        return categories.stream()
                .map(this::copy)
                .toList();
    }

    @Override
    public List<Category> findByQuery(CategoryQuery query) {
        return categories.stream()
                .map(this::copy)
                .toList();
    }

    @Override
    public Optional<Category> findLastCategory() {
        if (categories.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(copy(categories.getLast()));
    }

    @Override
    public Category save(Category category) {
        Category categoryToPersist = copy(category);
        findCategory(categoryToPersist)
                .ifPresentOrElse(
                        element -> element.setName(categoryToPersist.getName()),
                        () ->  {
                            categoryToPersist.setInternalId(nextId.getAndIncrement());
                            categories.add(categoryToPersist);
                        }
                );
        return copy(category);
    }

    @Override
    public Category moveToOrder(Category category, int targetOrder) {
        Optional<Category> element = findCategory(category);
        if (element.isEmpty() || targetOrder > categories.size()) {
            return category;
        }

        Category categoryToMove = element.get();

        if (categoryToMove.getOrder() == targetOrder) {
            return copy(category);
        }

        categories.remove(categoryToMove);
        categories.add(targetOrder - 1, categoryToMove);

        for (int i = 0; i < categories.size(); i++) {
            categories.get(i).setOrder(i + 1);
        }

        return copy(category);
    }

    private Optional<Category> findCategory(Category category) {
        return categories.stream()
                .filter(element -> Objects.equals(category.getInternalId(), element.getInternalId()))
                .findFirst();
    }

    private Category copy(Category source) {
        Category target = new Category();
        target.setInternalId(source.getInternalId());
        target.setName(source.getName());
        target.setOrder(source.getOrder());
        return target;
    }
}
