package me.korzeniak.myportal.shopping.domain.list.service

import me.korzeniak.myportal.shopping.domain.list.dto.CategoryCreateCommand
import me.korzeniak.myportal.shopping.domain.list.dto.CategoryReorderCommand
import me.korzeniak.myportal.shopping.domain.list.dto.CategoryUpdateCommand
import spock.lang.Specification

class CategoryServiceTest extends Specification {

    private final CategoryService categoryService = setupSubject()

    def "should create first category"() {
        given:
            def categoryName = "Meat"
            def newCategory = new CategoryCreateCommand(name: categoryName)
        when:
            categoryService.createCategory(newCategory)
        then:
            def categories = categoryService.getAllCategories()
            categories.size() == 1

            def createdCategory = categories[0]
            createdCategory.internalId != null
            createdCategory.name == categoryName
            createdCategory.order == 1
    }

    def "should create next category"() {
        given:
            def initialAmount = 15
            persistDummyCategories(initialAmount)

            def categoryName = "Meat"
            def newCategory = new CategoryCreateCommand(name: categoryName)
        when:
            categoryService.createCategory(newCategory)
        then:
            def categories = categoryService.getAllCategories()
            categories.size() == initialAmount + 1

            def createdCategory = categories[initialAmount]
            createdCategory.internalId != null
            createdCategory.name == categoryName
            createdCategory.order == initialAmount + 1
    }

    def "should update category details"() {
        given:
            persistDummyCategories(15)

            def categoryIndex = 7
            def category = categoryService.getAllCategories()[categoryIndex]

            def newCategoryName = "Milk"
            def categoryUpdateCommand = new CategoryUpdateCommand(setInternalId: category.internalId, name: newCategoryName)
        when:
            categoryService.updateCategory(categoryUpdateCommand)
        then:
            def updatedCategory = categoryService.getAllCategories()[categoryIndex]
            updatedCategory.internalId == category.internalId
            updatedCategory.name == newCategoryName
            updatedCategory.order == category.order
    }

    def "should not move category order"() {
        given:
            def amount = 15
            def sourceOrder = 8
            persistDummyCategories(amount)

            def sourceCategories = categoryService.getAllCategories()
            def category = fetchAtOrder(sourceCategories, sourceOrder)

            def categoryReorderCommand = new CategoryReorderCommand(id: category.id, order: sourceOrder)
        when:
            categoryService.reorderCategory(categoryReorderCommand)
        then:
            def resultCategories = categoryService.getAllCategories()

            for (int i = 0; i < amount; i++) {
                assert resultCategories[i].order == i + 1
                assert resultCategories[i].internalId == sourceCategories[i].internalId
            }
    }

    def "should move category further"() {
        given:
            def amount = 15
            persistDummyCategories(amount)

            def sourceCategories = categoryService.getAllCategories()
            def categoryToMove = sourceCategories[sourceOrder - 1]

            def categoryReorderCommand = new CategoryReorderCommand(id: categoryToMove.internalId, order: targetOrder)
        when:
            categoryService.reorderCategory(categoryReorderCommand)
        then:
            def resultCategories = categoryService.getAllCategories()

            for (int i = 0; i < amount; i++) {
                assert resultCategories[i].order == i + 1
            }

            for (int i = 0; i < sourceOrder - 1; i++) {
                assert resultCategories[i].internalId == sourceCategories[i].internalId
            }

            for (int i = sourceOrder - 1; i < targetOrder - 1; i++) {
                assert resultCategories[i].internalId == sourceCategories[i + 1].internalId
            }

            for (int i = targetOrder; i < resultCategories.size(); i++) {
                assert resultCategories[i].internalId == sourceCategories[i + 1].internalId
            }

            where:
                sourceOrder | targetOrder
                5 | 10
//                7 | 2
    }

    private static CategoryService setupSubject() {
        CategoryRepository categoryRepository = new SimpleCategoryRepository()
        new CategoryService(categoryRepository)
    }

    private void persistDummyCategories(int amount) {
        for (int i = 0; i < amount; i++) {
            CategoryCreateCommand command = new CategoryCreateCommand(name: "Dummy" + i)
            categoryService.createCategory(command)
        }
    }

    private static Category fetchAtOrder(List<Category> categories, int order) {
        categories[order - 1]
    }
}
